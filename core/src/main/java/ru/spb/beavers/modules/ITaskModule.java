package ru.spb.beavers.modules;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Интерфейс, который должны реализовывать модули
 * Позволяет обеспечить взаимодействие между модулями и ядром
 */
public interface ITaskModule {

    /**
     * @return Название задачи. 1 - 4 слова. 
     */
    public String getTitle();

    /**
     * Инициализация элементов на панели, дающей краткое описание решаемой задачи.
     * @param panel Инициализируемая панель.
     */
    public void initDescriptionPanel(JPanel panel);

    /**
     * Инициализация элементов на панели, предоставляющие подробное решение задачи.
     * Выводится полная теоритическая справка, вывод всех формул, графиков, описание входных/выходных данных задачи.
     * @param panel Инициализируемая панель.
     */
    public void initSolutionPanel(JPanel panel);

    /**
     * Инициализация элементов панели ввода исходных данных.
     * @param panel Инициализируемая панель.
     */
    public void initInputPanel(JPanel panel);

    /**
     * Инициализация элементов панели оторбражающей решение задачи с введенными исходными данными.
     * @param panel Инициализируемая панель.
     */
    public void initExamplePanel(JPanel panel);

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки.
     * @return Слушатель, который будет вызван для сохранения набора исходных данных в файл.
     */
    public ActionListener getPressSaveListener() throws IllegalArgumentException;

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для загрузки набора исходных данных из файла.
     */
    public ActionListener getPressLoadListener() throws IllegalArgumentException;

    /**
     * Listener может бросать IllegalArgumentException с описанием ошибки
     * @return Слушатель, который будет вызван для установки значений по умолчанию входных параметров задачи.
     */
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException;
}